$(window).bind("load", function() { 
       var footerHeight = 0,
           footerTop = 0,
           $footer = $("footer");
           
       positionFooter();
       
       function positionFooter() {
       
                footerHeight = $footer.outerHeight()+1;
                footerTop = ($(window).scrollTop()+$(window).height()-footerHeight)+"px";
       			
               if ( ($(document.body).height()+footerHeight) < $(window).height()) {
                   $footer.css({
                        position: "absolute"
                   }).stop().animate({
                        top: footerTop
                   },0)
               } else {
                   $footer.css({
                        position: "static"
                   })
               }
               
       }
	   
	   setTimeout(positionFooter, 500);

       $(window)
               .scroll(positionFooter)
               .resize(positionFooter)
               
});