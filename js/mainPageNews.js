$(document).ready(function(e) {
    
	var $area = $("#newsArea");
	var $next = $("#news-next");
	var $prev = $("#news-prev");
	var $elem = $(".newsSelf");
	var $elemLast = $elem.last();
	var count = $elem.length-1;
	var current = 0;
	var $currentElem;
	var currentHeight;
	var markup;
	
	function nextNews(){
		$currentElem = $elem.eq(current)
		currentHeight = $currentElem.height();
		$currentElem.stop()
		.animate({
			"margin-top":"-"+currentHeight+"px"
		},700, function(){
			current = currentPlusSelector(current);
			markup = $currentElem;
			markup.css({"margin-top":""});
			$area.append(markup);
		});
	}
	function prevNews(){
		$currentElem = $elem.eq(current)
		currentHeight = $currentElem.height();
		
		current = currentMinusSelector(current);
		$elemLast = $elem.eq(current);
		
		$elemLast.css({"margin-top":"-"+currentHeight+"px"});
		$area.prepend($elemLast);
		
		
		$elemLast.stop()
		.animate({
			"margin-top":""
		},700, function(){
			
			markup = $currentElem;
			markup.css({"margin-top":""});
			//$area.prepend(markup);
		});
	}
	function currentPlusSelector(current){
		if(current < count)
		{
			current++;
		}else{
			current=0;
		}
		
		return current
	};
	function currentMinusSelector(current){
		if(current > 0)
		{
			current--;
		}else{
			current=count;
		}
		
		return current
	};
	
	$prev.click(function(){
		prevNews();
	});
	$next.click(function(){
		nextNews();
	});
});