(function ( $ ) {
 
    /*$.fn.greenify = function( options ) {
 
        // This is the easiest way to have default options.
        var settings = $.extend({
            // These are the defaults.
            color: "#556b2f",
            backgroundColor: "white"
        }, options );
 
        // Greenify the collection based on the settings variable.
        return this.css({
            color: settings.color,
            backgroundColor: settings.backgroundColor
        });
 
    };*/
	
	$.fn.heightConnect = function(obj2, options){
		var $Obj = this;
		var $Obj2 = $("#"+obj2);
		
		var settings = $.extend({
            fromTo: "-",
			howMuch: 0
        }, options );
		
		var lastH;
		if(settings.fromTo == "-")
		{
			lastH = (parseInt($Obj2.height()) - parseInt(settings.howMuch))+'px';
		}else{
			lastH = (parseInt($Obj2.height()) + parseInt(settings.howMuch))+'px';
		}
		
		return $Obj.css({
			height: lastH
		});
	};
	
	
	$.fn.centerV = function(obj2){
		var $Obj = this;
		var $Obj2 = $("#"+obj2);
		var margin = (($Obj2.height()/2)-($Obj.height()/2))+'px';
		
		return $Obj.css({
			marginTop: margin
		});
	};
 
}( jQuery ));